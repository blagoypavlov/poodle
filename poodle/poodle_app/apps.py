from django.apps import AppConfig


class PoodleAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'poodle_app'
